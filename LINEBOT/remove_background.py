import os 
import glob 
from pathlib import Path
import datetime

def remove_background(image_path):
    now = datetime.datetime.now()
    picture_number = "%s%s%s%s%s%s"%(str(now.year).zfill(4),str(now.month).zfill(2),str(now.day).zfill(2),str(now.hour).zfill(2),str(now.minute).zfill(2),str(now.second).zfill(2))
    image_name = Path(image_path).parts[-1]
    output_path = f'./static/LINEBOT/remove_background/rb_{picture_number}_{image_name}'
    os.system(f'backgroundremover -i "{image_path}" -o "{output_path}"')
    return output_path
