from django.urls import path
from . import views

urlpatterns = [
    path('edit_location_info', views.edit_location_info),
    path('get_location_datas', views.get_location_datas),
    path('read_location_info', views.read_location_info)
]
