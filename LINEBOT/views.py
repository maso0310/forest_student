#=========Django的套件===========
from django.shortcuts import render

# Create your views here.
from django.conf import settings
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import FileSystemStorage
#=========Django的套件===========

#=========LINE的套件===========
from linebot import LineBotApi, WebhookParser
from linebot.exceptions import InvalidSignatureError, LineBotApiError
from linebot.models import *
from liffpy import (
    LineFrontendFramework as LIFF,
    ErrorResponse
)
#=========LINE的套件===========

#=========自己寫的code===========
from .config import *
from .models import *
from .flex_msg import *
from .richmenu_creator import *
#=========自己寫的code===========

#=========Python的套件===========
import sys
import traceback
import datetime
#=========Python的套件===========


liff_api = LIFF(CHANNEL_ACCESS_TOKEN)
line_bot_api = LineBotApi(CHANNEL_ACCESS_TOKEN)
parser = WebhookParser(CHANNEL_SECRET)


#從用戶現有座標去讀取附近地標資訊
@csrf_exempt
def read_location_info(request):
    return render(request, 'read_location_info.html', locals())


#從地標編號開啟地標資訊
@csrf_exempt
def edit_location_info(request):
    try:
        if request.method == 'GET':
            headers = request.headers
            for key, value in request.GET.lists():
                if key == 'location_id':
                    location_id = value[0]
            
            #查詢地標資料庫內容
            location = Location_Info.objects.get(id=location_id)
            location_data = {}
            location_data['Id'] = location_id
            location_data['title'] = location.title
            location_data['address'] = location.address
            location_data['lat'] = location.lat
            location_data['lng'] = location.lng
            location_data['text'] = location.text
            return render(request, 'edit_location_info.html', {"location_data": location_data})

    except Exception as e:
        Error_Logs.objects.create(error=traceback.format_exc())            
        return HttpResponseBadRequest()

#接收地圖資訊的資料
@csrf_exempt
def get_location_datas(request):
    try:
        if request.method == 'POST':
            for key, value in request.POST.lists():#這邊的key是依據input的name而定
                if key == 'location_info':
                    title = value[0]
                elif key == 'text_info':
                    text = value[0]
                elif key == 'locationId':
                    locationId = value[0]
            try:
                upload = request.FILES['upload_image']
                file_name = upload.name
                fss = FileSystemStorage()
                file = fss.save(f'./static/LINEBOT/upload_image/{locationId}_{file_name}', upload)
                image_url = APP_URL + fss.url(file)
                
            except Exception as e:
                Error_Logs.objects.create(error=traceback.format_exc())            
                return HttpResponseBadRequest()
            
            if image_url:
                Location_Info.objects.filter(id=locationId).update(title=title,text=text,image_url=image_url)

            return HttpResponse()

    except Exception as e:
        Error_Logs.objects.create(error=traceback.format_exc())            
        return HttpResponseBadRequest()
        
#處理LINEBOT的webhook event
@csrf_exempt
def callback(request):
    if request.method == 'POST':
        signature = request.META['HTTP_X_LINE_SIGNATURE']
        body = request.body.decode('utf-8')

        try:
            events = parser.parse(body, signature)
        except InvalidSignatureError:
            return HttpResponseForbidden()
        except LineBotApiError:
            return HttpResponseBadRequest()

        for event in events:
            uid=event.source.user_id
            profile=line_bot_api.get_profile(uid)
            name=profile.display_name
            pic_url=profile.picture_url
            
            #寫入會員資料
            if User_Info.objects.filter(uid=uid).exists()==False:
                User_Info.objects.create(uid=uid,name=name,pic_url=pic_url)                        

            if isinstance(event, MessageEvent):
                #建立回覆訊息列表
                message=[]

                if event.message.type=='text':
                    #解析文字訊息內容
                    mtext=event.message.text
                    print(f"文字訊息：{mtext}")
                    if mtext == '建立選單':
                        try:
                            #建立richmenu
                            set_richmenu()
                            message.append(TextSendMessage(text='完成建立選單'))
                            line_bot_api.reply_message(event.reply_token,message)
                        except:
                            message.append(TextSendMessage(text='建立選單錯誤'))
                            line_bot_api.reply_message(event.reply_token,message)

                elif event.message.type=='image':
                    print("圖片訊息")
                    image_content = line_bot_api.get_message_content(event.message.id)
                    now = datetime.datetime.now()
                    picture_number = "%s%s%s%s%s%s"%(str(now.year).zfill(4),str(now.month).zfill(2),str(now.day).zfill(2),str(now.hour).zfill(2),str(now.minute).zfill(2),str(now.second).zfill(2))
                    image_name = f'tmp_{picture_number}_{uid}.jpg'
                    print(image_name)
                    path=image_name#int(image_name[0].id)+1)+'.jpg'
                    with open(path, 'wb') as fd:
                        for chunk in image_content.iter_content():
                            fd.write(chunk)
                    try_to_open_image = open(path,'rb')
                    print(try_to_open_image)
                    status = User_Info.objects.get(uid=uid).status
                    if status == '一般模式':
                        print('一般模式')
                    elif status == '綠覆率模式':
                        print('綠覆率模式')
                        response = requests.post('https://uav-fly.nchu.edu.tw/green_cover',files={"file":open(path,'rb')})
                        print(response)
                        print(response.json())
                        message.append(ImageSendMessage(original_content_url='https://uav-fly.nchu.edu.tw/' + response.json()['green_image_url'][1:],preview_image_url='https://uav-fly.nchu.edu.tw/' + response.json()['green_image_url'][1:]))

                    elif status == '去背模式':
                        print('去背模式')
                        print(response.json())
                        response = requests.post('https://uav-fly.nchu.edu.tw/remove_background',files={"file":open(path,'rb')})
                        message.append(ImageSendMessage(original_content_url='https://uav-fly.nchu.edu.tw/' + response.json()['rb_image_url'][1:],preview_image_url='https://uav-fly.nchu.edu.tw/' + response.json()['rb_image_url'][1:]))
                        
                    line_bot_api.reply_message(event.reply_token,message)

                elif event.message.type=='location':
                    """
                    TODO:
                    1.收到位置訊息之後，選擇是否要新增該位置訊息的屬性資料(圖片/文字)
                    """
                    
                    #解析座標位置資訊
                    lng = event.message.longitude
                    lat = event.message.latitude
                    try:title = event.message.title
                    except:title = '自訂地標'
                    if title == None:title = '自訂地標'
                    try:address = event.message.address
                    except:address = '地址資訊'
                    
                    #建立地標資料
                    Location_Info.objects.create(uid=uid,name=name,title=title,address=address,lat=lat,lng=lng)

                    #建立回傳訊息
                    message.append(add_to_map(Id=len(Location_Info.objects.all()),title=title, lat=lat, lng=lng, address=address))
                    line_bot_api.reply_message(event.reply_token,message)

                elif event.message.type=='video':
                    message.append(TextSendMessage(text='影片訊息'))
                    line_bot_api.reply_message(event.reply_token,message)


                elif event.message.type=='sticker':
                    message.append(TextSendMessage(text='貼圖訊息'))
                    line_bot_api.reply_message(event.reply_token,message)

                elif event.message.type=='audio':
                    message.append(TextSendMessage(text='聲音訊息'))
                    line_bot_api.reply_message(event.reply_token,message)

                elif event.message.type=='file':
                    message.append(TextSendMessage(text='檔案訊息'))
                    line_bot_api.reply_message(event.reply_token,message)

            elif isinstance(event, FollowEvent):
                line_bot_api.reply_message(event.reply_token,message)

            elif isinstance(event, UnfollowEvent):
                pass

            elif isinstance(event, JoinEvent):
                pass

            elif isinstance(event, LeaveEvent):
                pass

            elif isinstance(event, MemberJoinedEvent):
                pass

            elif isinstance(event, MemberLeftEvent):
                pass

            elif isinstance(event, PostbackEvent):
                data = event.postback.data
                print(data)
                if data == '新增地標':
                    message = TextSendMessage("是否新增地標？",quick_reply=QuickReply(items=[
                                QuickReplyButton(action=LocationAction(label="新增地標")),
                                QuickReplyButton(action=MessageAction(label="取消",text="取消新增地標"))
                            ]))
                    line_bot_api.reply_message(event.reply_token,message)

                elif '狀態切換：' in data:
                    status = data.split('：')[1]
                    User_Info.objects.filter(uid=uid).update(status=status)
                    message = TextSendMessage(f'圖像處理狀態切換為：{status}')
                    line_bot_api.reply_message(event.reply_token,message)


        return HttpResponse()
    else:
        return HttpResponseBadRequest()
