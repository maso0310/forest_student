from linebot import LineBotApi, WebhookParser
from linebot.models import *
from .config import *

from pathlib import Path
import requests
import json

line_bot_api = LineBotApi(CHANNEL_ACCESS_TOKEN)


def set_richmenu():    
    rich_menu_list = line_bot_api.get_rich_menu_list()

    if len(rich_menu_list) == 2:
        return "Well Done"

    if len(rich_menu_list) != 0:
        for rich_menu in rich_menu_list:
            rich_menu_id = rich_menu.rich_menu_id
            line_bot_api.delete_rich_menu(rich_menu_id)

    rich_menu_list = line_bot_api.get_rich_menu_list()

    if len(rich_menu_list) == 0:
        #建立richmenu
        headers = {
            'Authorization': f'Bearer {CHANNEL_ACCESS_TOKEN}',
            'Content-Type': f'application/json'
        }

        create_richmenu_url = 'https://api.line.me/v2/bot/richmenu'
        create_richmenu_alias_url = 'https://api.line.me/v2/bot/richmenu/alias'
        
        richmenu_path_list = [
            f'./LINEBOT/json/richmenu_camera.json',
            f'./LINEBOT/json/richmenu_map.json'
        ]
        
        richmenu_image_path_list = [
            f'./static/LINEBOT/richmenu/camera.png',
            f'./static/LINEBOT/richmenu/map.png'            
        ]

        for richmaun_path, richmenu_image_path in zip(richmenu_path_list, richmenu_image_path_list):
            data = json.load(open(richmaun_path, encoding="utf8"))
            response = requests.post(create_richmenu_url, headers=headers, json=data)
            print(response.json())


            rich_menu_id = response.json()['richMenuId']

            
            with open(richmenu_image_path, 'rb') as f:
                line_bot_api.set_rich_menu_image(rich_menu_id, 'image/png', f)

            alias_name = Path(richmenu_image_path).stem
            delete_ailas_url = f'https://api.line.me/v2/bot/richmenu/alias/{alias_name}'
            response = requests.delete(delete_ailas_url, headers={'Authorization': f'Bearer {CHANNEL_ACCESS_TOKEN}'})
            data = {
                "richMenuId": rich_menu_id,
                "richMenuAliasId": f"{alias_name}",
            }
            response = requests.post(create_richmenu_alias_url, headers=headers, json=data)
                
            set_default_rich_menu_api_url = f'https://api.line.me/v2/bot/user/all/richmenu/{rich_menu_id}'

            headers = {
                'Authorization': f'Bearer {CHANNEL_ACCESS_TOKEN}'
            }
            response = requests.post(set_default_rich_menu_api_url, headers=headers)
