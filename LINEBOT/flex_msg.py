from linebot.models import *
from .config import *

#新增資訊到地圖
def add_to_map(**kwargs):
    Id = kwargs['Id']
    lat = kwargs['lat']
    lng = kwargs['lng']
    address = kwargs['address']
    title = kwargs['title']
    print(lat, lng, address)
    contents = {"type": "bubble",
                "hero": {
                    "type": "image",
                    "url": "https://i.imgur.com/YKYdhLk.png",
                    "size": "full",
                    "aspectRatio": "20:13",
                    "aspectMode": "fit",
                    "action": {
                        "type": "uri",
                        "uri": "http://linecorp.com/"
                    }
                },
                "body": {
                    "type": "box",
                    "layout": "vertical",
                    "contents": [{
                            "type": "text",
                            "text": "已新增了一個地標資訊",
                            "weight": "bold",
                            "size": "xl"
                        },
                        {
                            "type": "text",
                            "text": f"{title}",
                            "margin": "lg"
                        },
                        {
                            "type": "box",
                            "layout": "baseline",
                            "margin": "lg",
                            "contents": [{
                                    "type": "icon",
                                    "size": "sm",
                                    "url": "https://cdn-icons-png.flaticon.com/512/684/684908.png"
                                },
                                {
                                    "type": "text",
                                    "text": f"{address}",
                                    "size": "sm",
                                    "color": "#999999",
                                    "margin": "md",
                                    "flex": 0
                                }
                            ]
                        },
                        {
                            "type": "box",
                            "layout": "vertical",
                            "margin": "lg",
                            "spacing": "sm",
                            "contents": [{
                                    "type": "box",
                                    "layout": "baseline",
                                    "spacing": "sm",
                                    "contents": [{
                                            "type": "text",
                                            "text": "緯度",
                                            "color": "#aaaaaa",
                                            "size": "sm",
                                            "flex": 1
                                        },
                                        {
                                            "type": "text",
                                            "text": f"{lat}",
                                            "wrap": True,
                                            "color": "#666666",
                                            "size": "sm",
                                            "flex": 5
                                        }
                                    ]
                                },
                                {
                                    "type": "box",
                                    "layout": "baseline",
                                    "spacing": "sm",
                                    "contents": [{
                                            "type": "text",
                                            "text": "經度",
                                            "color": "#aaaaaa",
                                            "size": "sm",
                                            "flex": 1
                                        },
                                        {
                                            "type": "text",
                                            "text": f"{lng}",
                                            "wrap": True,
                                            "color": "#666666",
                                            "size": "sm",
                                            "flex": 5
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                "footer": {
                    "type": "box",
                    "layout": "vertical",
                    "spacing": "sm",
                    "contents": [{
                            "type": "button",
                            "style": "secondary",
                            "height": "sm",
                            "action": {
                                "type": "uri",
                                "label": "編輯地標屬性資料",
                                "uri": f"{APP_URL}/MasterMaso/edit_location_info?location_id={Id}"
                            },
                            "color": "#A8EEFC"
                        },
                        {
                            "type": "button",
                            "action": {
                                "type": "uri",
                                "label": "查看地標屬性資料",
                                "uri": "http://linecorp.com/"
                            },
                            "color": "#A8EEFC",
                            "style": "secondary"
                        },
                        {
                            "type": "button",
                            "style": "secondary",
                            "height": "sm",
                            "action": {
                                "type": "uri",
                                "label": "刪除此地標",
                                "uri": "https://linecorp.com"
                            },
                            "color": "#A8EEFC"
                        },
                        {
                            "type": "box",
                            "layout": "vertical",
                            "contents": [],
                            "margin": "sm"
                        }
                    ],
                    "flex": 0
                }
            }
    message = FlexSendMessage(alt_text='上傳地圖資訊',contents=contents)
    return message
    